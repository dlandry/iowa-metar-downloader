import hydra
import importlib
import luigi
import pandas as pd
import pathlib
import pymongo

from luigi.contrib.mongodb import MongoCollectionTarget

from .crawler import crawl_one_station_collection, crawl_one_station_parquet

class MyMongoCollectionTarget(MongoCollectionTarget):
    """Need to fix an outdated call to PyMongo"""

    def __init__(self, client, database_name, collection_name, skip_check=False):
        super().__init__(client, database_name, collection_name)
        self.skip_check = skip_check

    def read(self):
        """
        Return if the target collection exists in the database
        """
        return self._collection in self.get_index().list_collection_names()

    def exists(self):
        return self.skip_check or super().exists()


class FetchMetarOfStation(luigi.Task):
    begin = luigi.DateParameter()
    end = luigi.DateParameter()
    station_name = luigi.Parameter()
    mongo_uri = luigi.Parameter()
    database_name = luigi.Parameter()
    skip_check = luigi.BoolParameter(default=False)

    def output(self):
        client = pymongo.MongoClient(str(self.mongo_uri))

        collection_name = f"stn_{self.station_name}"
        return MyMongoCollectionTarget(
            client, self.database_name, collection_name, skip_check=self.skip_check
        )

    def run(self):
        print('Mongo URI', self.mongo_uri)

        collection = self.output().get_collection()
        collection.create_index("valid", unique=True)

        crawl_one_station_collection(
            self.station_name, self.begin, self.end, collection
        )

    def requires(self):
        return []


class FetchMetar(luigi.WrapperTask):
    def requires(self):
        station_file = importlib.resources.files("metardownloader").joinpath("stations.csv")
        stations_df = pd.read_csv(str(station_file))

        return [FetchMetarOfStation(station_name=s) for s in stations_df["station"]]



class CreateStationParquet(luigi.Task):
    begin = luigi.DateParameter()
    end = luigi.DateParameter()
    station_name = luigi.Parameter()
    output_dir = luigi.PathParameter()

    def output_path(self):
        return pathlib.Path(str(self.output_dir)) / f"{self.station_name}.parquet"

    def output(self):
        return luigi.LocalTarget(self.output_path())
    
    def run(self):
        self.output_path().parent.mkdir(exist_ok=True, parents=True)
        crawl_one_station_parquet(self.station_name, self.begin, self.end, self.output_path())

    def requires(self):
        return []
    

class CreateParquet(luigi.WrapperTask):
    output_dir = luigi.PathParameter()
    begin = luigi.DateParameter()
    end = luigi.DateParameter()

    def requires(self):
        station_file = importlib.resources.files("metardownloader").joinpath("stations.csv")
        stations_df = pd.read_csv(str(station_file))

        return [CreateStationParquet(station_name=s, output_dir=self.output_dir, begin=self.begin, end=self.end) for s in stations_df["station"]]   


@hydra.main(config_path='conf', config_name='config', version_base='1.3')
def cli(cfg):
    luigi.build([FetchMetar()], workers=cfg.n_workers, scheduler_host=cfg.scheduler_host)