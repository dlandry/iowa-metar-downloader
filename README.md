# IOWA State METAR Downloader

Fetches data from the Iowa State University MESONET archive.
See https://mesonet.agron.iastate.edu/request/download.phtml.
Currently fetches data from a selection of stations in Canada and the US, but could be configured otherwise.

## Installation

```
pip install .
```

## Usage

1. Modify the `luigi.cfg` file in the root.
   Notably, make sure the database connection information are correct.
2. The list of stations to fetch is the `stations.csv` file, point the script to another file if needed.

Then, you can start the script either through the `luigi` executable `hydra`.
Going through `luigi` gives more configuration options.
Going through `hydra` makes it easier to use SLURM.

Example luigi command
```
LUIGI_CONFIG_PATH=<path_to_cfg> luigi --module metardownloader.luigi FetchMetarOfStation --station-name CYUL --local-scheduler
```

Example Hydra command
```
LUIGI_CONFIG_PATH=<path_to_cfg> download_metar hydra/launcher=cleps -m 
```